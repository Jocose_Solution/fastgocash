﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using InstantPayServiceLib;
using Newtonsoft.Json.Linq;

public partial class DMT_Manager_payout_direct_fund_transfer : System.Web.UI.Page
{
    private static string UserId { get; set; }
    private static string Mobile { get; set; }
    private static string RemitterId { get; set; }
    public string RemitterDetail { get; set; }
    public string RemitterBenDetail { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UID"] != null && !string.IsNullOrEmpty(Session["UID"].ToString()))
        {
            UserId = Session["UID"].ToString();

            Mobile = Request.QueryString["mobile"] != null ? Request.QueryString["mobile"].ToString() : string.Empty;
            RemitterId = Request.QueryString["sender"] != null ? Request.QueryString["sender"].ToString() : string.Empty;

            if (string.IsNullOrEmpty(UserId) && string.IsNullOrEmpty(Mobile) && string.IsNullOrEmpty(RemitterId))
            {
                Response.Redirect("");
            }
            else if (string.IsNullOrEmpty(Mobile) && string.IsNullOrEmpty(RemitterId))
            {
                Response.Redirect("/dmt-manager/payoutdirect.aspx");
            }
            else
            {
                BindRemitterDetails(UserId, Mobile, RemitterId);

                //if (Session["RemtIdPayoutSearch"] != null)
                //{
                //    BindRemitterDetails(UserId, Mobile, RemitterId);
                //}
                //else
                //{
                //    Response.Redirect("/dmt-manager/payoutdirect.aspx");
                //}
            }
        }
        else
        {
            Response.Redirect("/");
        }
    }

    private void BindRemitterDetails(string agentId, string regmobile, string remitterid)
    {
        List<string> result = new List<string>();

        if (!string.IsNullOrEmpty(agentId))
        {
            if (!string.IsNullOrEmpty(regmobile) && !string.IsNullOrEmpty(remitterid))
            {
                DataTable dtRemitter = InstantPay_ApiService.GetT_InstantPayRemitterRegResponse(agentId, remitterid, regmobile);
                if (dtRemitter != null && dtRemitter.Rows.Count > 0)
                {
                    string sendername = dtRemitter.Rows[0]["name"].ToString();
                    string mobile = dtRemitter.Rows[0]["mobile"].ToString();
                    string address = dtRemitter.Rows[0]["address"].ToString() + ", " + dtRemitter.Rows[0]["state"].ToString() + ", " + dtRemitter.Rows[0]["pincode_res"].ToString();
                    string creditLimit = dtRemitter.Rows[0]["CreditLimit"].ToString();
                    string remainingLimit = dtRemitter.Rows[0]["remaininglimit"].ToString();
                    string consumedAmount = dtRemitter.Rows[0]["consumedlimit"].ToString();
                    string localAddress = !string.IsNullOrEmpty(dtRemitter.Rows[0]["CurrentAddress"].ToString()) ? dtRemitter.Rows[0]["CurrentAddress"].ToString() : "- - -";

                    RemitterDetail = SenderHTMLDetails(sendername, mobile, address, localAddress, creditLimit, remainingLimit, consumedAmount);
                }
            }
        }
    }

    public static string SenderHTMLDetails(string sendername, string mobile, string address, string localadd, string creditLimit, string remainingLimit, string consumedAmount)
    {
        StringBuilder sbSender = new StringBuilder();

        sbSender.Append("<div class='row boxSender'>");
        sbSender.Append("<div class='col-sm-6'><p class='p_botm'>Sender Details</p></div>");
        sbSender.Append("<div class='col-sm-6' style='padding: 2px;'><div class='row'><div class='col-sm-6'><p class='p_botm'>" + sendername.ToUpper() + " (" + mobile + ")</p></div><div class='col-sm-6 text-right'><p class='p_botm btn btn-sm' style='border: 1px solid #fff;color: #fff;cursor: pointer;padding: 6px;' id='UpdateLocalAddress'><i class='fa fa-edit'></i> Edit local Address</p></div></div></div>");
        sbSender.Append("</div>");

        sbSender.Append("<div class='row boxSenderdetail'>");
        sbSender.Append("<div class='col-sm-6'><p class='pmargin'>Name</p><p class='pmargin'>Address</p><p class='pmargin'>Current / Local Address</p></div>");
        sbSender.Append("<div class='col-sm-6'><p class='pmargin'>" + sendername.ToUpper() + " (" + mobile + ")</p><p class='pmargin'>" + address + "</p><p class='pmargin'>" + localadd + "</p></div>");
        sbSender.Append("</div>");

        return sbSender.ToString();
    }

    [WebMethod]
    public static List<string> GetPayoutTransactionHistory(string fromdate, string todate, string trackid, string filstatus, string type)
    {
        List<string> result = new List<string>();

        try
        {
            if (!string.IsNullOrEmpty(UserId))
            {
                if (!string.IsNullOrEmpty(Mobile) && !string.IsNullOrEmpty(RemitterId))
                {
                    if (string.IsNullOrEmpty(fromdate))
                    {
                        DateTime currDate = DateTime.Now;
                        fromdate = currDate.ToString("dd/MM/yyyy");
                    }

                    DataTable dtTrans = InstantPay_ApiService.GetPayoutTransactionHistory(RemitterId, UserId, fromdate, todate, trackid, filstatus);

                    result.Add("success");
                    result.Add(BindTransDetails(dtTrans, type));
                }
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }

        return result;
    }

    private static string BindTransDetails(DataTable dtTrans, string datepos)
    {
        StringBuilder sbTrans = new StringBuilder();

        if (dtTrans != null && dtTrans.Rows.Count > 0)
        {
            for (int i = 0; i < dtTrans.Rows.Count; i++)
            {
                string refund_html = "- - -";
                string status = dtTrans.Rows[i]["Status"].ToString();

                if (status.ToLower().Trim() == "transaction under process")
                {
                    status = "<td class='text-warning'>UNDER PROCESS</td>";
                }
                else if (status.ToLower().Trim() == "transaction successful")
                {
                    status = "<td class='text-success'>SUCCESS</td>";
                }
                else if (status.ToLower().Trim() == "duplicate transaction")
                {
                    status = "<td class='text-danger'>DUPLICATE</td>";
                }
                else if (status.ToLower().Trim() == "transaction failed")
                {
                    status = "<td class='text-danger'>FAILED</td>";
                }
                else
                {
                    status = "<td>--</td>";
                }

                sbTrans.Append("<tr>");
                sbTrans.Append("<td>" + ConvertStringDateToStringDateFormate(dtTrans.Rows[i]["CreatedDate"].ToString()) + "</td>");
                sbTrans.Append("<td>" + dtTrans.Rows[i]["ipay_id"].ToString() + "</td>");
                //sbTrans.Append("<td>" + dtTrans.Rows[i]["orderid"].ToString() + "</td>");
                //sbTrans.Append("<td>" + dtTrans.Rows[i]["ref_no"].ToString() + "</td>");
                sbTrans.Append("<td>" + dtTrans.Rows[i]["TrackId"].ToString() + "</td>");

                string sp_key = dtTrans.Rows[i]["sp_key"].ToString();
                string transmode = string.Empty;
                if (sp_key == "DPN") { transmode = "IMPS"; }
                else if (sp_key == "BPN") { transmode = "NEFT"; }
                else if (sp_key == "CPN") { transmode = "RTGS"; }

                sbTrans.Append("<td>" + transmode + "</td>");
                sbTrans.Append("<td>₹ " + dtTrans.Rows[i]["transfer_value"].ToString() + "</td>");
                sbTrans.Append("<td>₹ " + dtTrans.Rows[i]["charged_amt"].ToString() + "</td>");
                sbTrans.Append("<td><span class='text-primary'>" + dtTrans.Rows[i]["bene_name"].ToString() + "</span></td>");
                sbTrans.Append(status);
                sbTrans.Append("<td>" + refund_html + "</td>");

                string url = "/dmt-manager/print-payout-receipt.aspx?mobile=" + Mobile + "&sender=" + RemitterId + "&trackid=" + dtTrans.Rows[i]["TrackId"].ToString() + "";

                sbTrans.Append("<td><a href='" + url + "' target='_blank' class='text-primary'>Print</a></td>");
                sbTrans.Append("</tr>");
            }
        }
        else
        {
            if (datepos == "today")
            {
                sbTrans.Append("<tr>");
                sbTrans.Append("<td colspan='12' class='text-danger text-center'>Today's record not found !</td>");
                sbTrans.Append("</tr>");
            }
            else
            {
                sbTrans.Append("<tr>");
                sbTrans.Append("<td colspan='12' class='text-danger text-center'>Record not found !</td>");
                sbTrans.Append("</tr>");
            }
        }

        return sbTrans.ToString();
    }

    public static string ConvertStringDateToStringDateFormate(string date)
    {
        DateTime dtDate = new DateTime();

        if (!string.IsNullOrEmpty(date))
        {
            dtDate = DateTime.Parse(date);
            return dtDate.ToString("dd MMM yyyy hh:mm tt");
        }

        return string.Empty;
    }

    [WebMethod]
    public static List<string> UpdateLocalAddress(string updatedaddress)
    {
        List<string> result = new List<string>();

        if (!string.IsNullOrEmpty(updatedaddress.Trim()))
        {
            if (!string.IsNullOrEmpty(UserId))
            {
                if (!string.IsNullOrEmpty(Mobile) && !string.IsNullOrEmpty(RemitterId))
                {
                    if (InstantPay_ApiService.UpdateLocalAddress(RemitterId, UserId, Mobile, updatedaddress))
                    {
                        result.Add("success");
                        result.Add("Address has been updated successfully.");
                    }
                }
            }
        }
        else
        {
            result.Add("empty");
            result.Add("Please enter current / local address !");
        }

        return result;
    }

    [WebMethod]
    public static List<string> ProcessToFundTransSendOTP(string sp_key, string amount)
    {
        List<string> result = new List<string>();

        if (!string.IsNullOrEmpty(sp_key.Trim()) && !string.IsNullOrEmpty(amount.Trim()))
        {
            if (!string.IsNullOrEmpty(UserId))
            {
                if (!string.IsNullOrEmpty(Mobile) && !string.IsNullOrEmpty(RemitterId))
                {
                    string response = InstantPay_ApiService.InitiatePayoutSendOTP(sp_key, amount, UserId);
                    //string response = "{'statuscode': 'TXN','status': 'OTP Sent successfully','data': {'registered_mobile': 'xxxx3317','registered_email': 'gaxxxx@xxxx.com'},'timestamp':'2020-09-01 13:48:29','ipay_uuid': '3AB6992D6193A8627D87','orderid': [],'environment': 'PRODUCTION'}";

                    if (!string.IsNullOrEmpty(response))
                    {
                        dynamic dyResult = JObject.Parse(response);
                        string statusCode = dyResult.statuscode;
                        string statusMessage = dyResult.status;

                        if (statusCode.ToLower() == "txn" && statusMessage.ToLower() == "otp sent successfully")
                        {
                            dynamic dyData = dyResult.data;
                            string registered_mobile = dyData.registered_mobile;
                            string registered_email = dyData.registered_email;

                            result.Add("success");
                            result.Add("OTP Sent successfully to registered mobile : " + registered_mobile + " and registered email : " + registered_email + "");
                        }
                        else
                        {
                            result.Add("failed");
                            result.Add(statusMessage);
                        }
                    }
                }
            }
            else
            {
                result.Add("reload");
            }
        }

        return result;
    }

    [WebMethod]
    public static List<string> ProcessToDirectFundTransfer(string accountno, string bankname, string ifsccode, string sp_key, string benname, string alert_mobile, string alert_email, string remark, string amount, string otp)
    {
        List<string> result = new List<string>();

        try
        {
            if (!string.IsNullOrEmpty(UserId))
            {
                if (!string.IsNullOrEmpty(Mobile) && !string.IsNullOrEmpty(RemitterId))
                {
                    if (!string.IsNullOrEmpty(otp.Trim()))
                    {
                        string agencyName = string.Empty; string agencyCreditLimit = string.Empty;

                        DataTable dtAgency = InstantPay_ApiService.GetAgencyDetailById(UserId);

                        if (dtAgency != null && dtAgency.Rows.Count > 0)
                        {
                            agencyName = dtAgency.Rows[0]["Agency_Name"].ToString();
                            agencyCreditLimit = dtAgency.Rows[0]["Crd_Limit"].ToString();

                            if (Convert.ToDouble(agencyCreditLimit) > 0)
                            {
                                string clientRefId = "IPFT" + Guid.NewGuid().ToString().Replace("-", "").Substring(0, 15).ToUpper();

                                //debit
                                //SqlTransactionDom objDom = new SqlTransactionDom();
                                //int isLedger = objDom.Ledgerandcreditlimit_Transaction(UserId, Convert.ToDouble(amount), trackid, "", "", agencyName, GetLocalIPAddress(), "", "", "", Convert.ToDouble(agencyCreditLimit.Trim()), "DMT_Fund_Transfer");

                                int isLedger = 1;

                                if (isLedger > 0)
                                {
                                    string latitude = "28.690430";
                                    string longitude = "77.101662";
                                    string ipaddress = GetLocalIPAddress();

                                    alert_mobile = !string.IsNullOrEmpty(alert_mobile) ? alert_mobile : Mobile;
                                    alert_email = !string.IsNullOrEmpty(alert_email) ? alert_email : "";

                                    string response = InstantPay_ApiService.ProcessToDirectFundTransfer(UserId, Mobile, RemitterId, accountno, bankname, ifsccode, clientRefId, sp_key, benname, alert_mobile, alert_email, remark, amount, otp, latitude, longitude, ipaddress);

                                    if (!string.IsNullOrEmpty(response))
                                    {
                                        dynamic dyResult = JObject.Parse(response);
                                        string statusCode = dyResult.statuscode;
                                        string statusMessage = dyResult.status;

                                        if (statusCode.ToLower() == "txn" && statusMessage.ToLower() == "transaction successful")
                                        {
                                            dynamic dyData = dyResult.data;

                                            string orderid = dyResult.orderid;
                                            string trackid = dyData.external_ref;

                                            result.Add("success");
                                            result.Add(orderid);
                                            result.Add(trackid);

                                            string mailBody = BindTransPrint(trackid);
                                            MailSend(mailBody, trackid);
                                        }
                                        else
                                        {
                                            result.Add("failed");
                                            result.Add(statusMessage);
                                        }
                                    }
                                }
                                else
                                {
                                    result.Add("failed");
                                    result.Add("We are unable to transfer money at the moment. Instead of trying again, please contact our call centre to avoid any inconvenience.");
                                }
                            }
                            else
                            {
                                result.Add("failed");
                                result.Add("Insufficient credit limit !");
                            }
                        }
                    }
                    else
                    {
                        result.Add("failed");
                        result.Add("Please enter otp !");
                    }
                }
            }
            else
            {
                result.Add("reload");
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }

        return result;
    }

    public static string GetLocalIPAddress()
    {
        string ipAddress = string.Empty;

        var host = Dns.GetHostEntry(Dns.GetHostName());
        foreach (var ip in host.AddressList)
        {
            if (ip.AddressFamily == AddressFamily.InterNetwork)
            {
                ipAddress = ip.ToString();
            }
        }

        return ipAddress;
    }

    #region [Email Formate -- Sending Mail]

    private static bool MailSend(string mailBody, string TrackId)
    {
        try
        {
            if (!string.IsNullOrEmpty(UserId))
            {
                if (!string.IsNullOrEmpty(Mobile) && !string.IsNullOrEmpty(RemitterId))
                {
                    DataTable dtAgency = InstantPay_ApiService.GetAgencyDetailById(UserId);

                    if (dtAgency != null && dtAgency.Rows.Count > 0)
                    {
                        string toEmail = dtAgency.Rows[0]["Email"].ToString();

                        SqlTransactionDom STDOM = new SqlTransactionDom();
                        DataTable MailDt = new DataTable();
                        MailDt = STDOM.GetMailingDetails("MONEY_TRANSFER", UserId).Tables[0];
                        if (MailDt != null && MailDt.Rows.Count > 0)
                        {
                            bool Status = Convert.ToBoolean(MailDt.Rows[0]["Status"].ToString());
                            string subject = "Money Transfer Receipt [Client Ref# " + TrackId + "]";
                            if (Status)
                            {
                                int isSuccess = STDOM.SendMail(toEmail, MailDt.Rows[0]["MAILFROM"].ToString(), MailDt.Rows[0]["BCC"].ToString(), MailDt.Rows[0]["CC"].ToString(), MailDt.Rows[0]["SMTPCLIENT"].ToString(), MailDt.Rows[0]["UserId"].ToString(), MailDt.Rows[0]["Pass"].ToString(), mailBody, subject, "");
                                if (isSuccess > 0)
                                {
                                    return true;
                                }
                                else
                                {
                                    return false;
                                }
                            }
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }

        return false;
    }

    private static string BindTransPrint(string trackid)
    {
        StringBuilder sbResult = new StringBuilder();

        if (!string.IsNullOrEmpty(UserId))
        {
            if (!string.IsNullOrEmpty(Mobile) && !string.IsNullOrEmpty(RemitterId))
            {
                //DataTable dtAgency = InstantPayFun.GetAgencyDetailById(UserId);

                DataTable dtRemitter = InstantPay_ApiService.GetCombieRemitterDetail(UserId, RemitterId, Mobile);

                sbResult.Append("<div style='border: 1px solid #ff414d; width: 800px; border-collapse: initial;' class='main'>");
                sbResult.Append("<div class='col-sm-12'>");
                sbResult.Append("<a href='#'><img src='../Images/gallery/logo(ft).png' style='max-width: 200px;' /></a>");
                sbResult.Append("<span style='float: right!important; text-align: right; padding: 7px;'>");
                sbResult.Append("<span>" + dtRemitter.Rows[0]["FirstName"].ToString() + " " + dtRemitter.Rows[0]["LastName"].ToString() + "</span><br />");
                sbResult.Append("<span>" + dtRemitter.Rows[0]["Address"].ToString() + ", " + dtRemitter.Rows[0]["City"].ToString() + ", " + dtRemitter.Rows[0]["State"].ToString() + ", " + dtRemitter.Rows[0]["PinCode"].ToString() + "</span>");
                sbResult.Append("</span>");
                sbResult.Append("<hr style='text-align: center; border: 1px solid #ff414d; background-color: #ff414d; margin-top: 0px; margin-bottom: 0;' />");
                sbResult.Append("</div>");
                sbResult.Append("<div class='col-sm-12'>");
                sbResult.Append("<h4 style='text-align: center;'>Customer Transaction Receipt</h4>");
                sbResult.Append("<table data-toggle='table' style='width: 800px; border-collapse: initial; border: 1px solid #ff414d; font-family: Verdana,Geneva,sans-serif; font-size: 12px; border-spacing: 0px; padding: 0px;'>");

                sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Sender Name</th>");
                sbResult.Append("<td colspan='3' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtRemitter.Rows[0]["FirstName"].ToString() + " " + dtRemitter.Rows[0]["LastName"].ToString() + "</td>");
                sbResult.Append("</tr>");

                sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Sender Mobile Number</th>");
                sbResult.Append("<td colspan='3' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtRemitter.Rows[0]["Mobile"].ToString() + "</td>");
                sbResult.Append("</tr>");

                DataTable dtTrans = InstantPay_ApiService.GetPayoutTransactionHistoryByTrackId(trackid);

                sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Beneficiary Name</th>");
                sbResult.Append("<td colspan='3' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtTrans.Rows[0]["bene_name"].ToString() + "</td>");
                sbResult.Append("</tr>");

                sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Beneficiary Account Number</th>");
                sbResult.Append("<td colspan='3' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtTrans.Rows[0]["payout_account"].ToString() + "</td>");
                sbResult.Append("</tr>");

                sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Beneficiary Bank's IFSC</th>");
                sbResult.Append("<td colspan='3' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtTrans.Rows[0]["payout_ifsc"].ToString() + "</td>");
                sbResult.Append("</tr>");

                sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Bank</th>");
                sbResult.Append("<td colspan='3' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtTrans.Rows[0]["bank_name"].ToString() + "</td>");
                sbResult.Append("</tr>");

                sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Order ID</th>");
                sbResult.Append("<td colspan='3' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtTrans.Rows[0]["orderid"].ToString() + "</td>");
                sbResult.Append("</tr>");

                //sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                //sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Transaction ID</th>");
                //sbResult.Append("<td colspan='2' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtTrans.Rows[0]["ipay_id"].ToString() + "</td>");
                //sbResult.Append("</tr>");

                sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Track ID</th>");
                sbResult.Append("<td colspan='3' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtTrans.Rows[0]["TrackId"].ToString() + "</td>");
                sbResult.Append("</tr>");

                sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Transaction Amount</th>");
                sbResult.Append("<td colspan='3' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>₹ " + dtTrans.Rows[0]["transfer_value"].ToString() + "</td>");
                sbResult.Append("</tr>");

                //sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                //sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Charged Amount</th>");
                //sbResult.Append("<td colspan='2' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtTrans.Rows[0]["charged_amt"].ToString() + "</td>");
                //sbResult.Append("</tr>");

                sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Transaction Date & Times</th>");
                sbResult.Append("<td colspan='3' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + ConvertStringDateToStringDateFormate(dtTrans.Rows[0]["CreatedDate"].ToString()) + "</td>");
                sbResult.Append("</tr>");

                sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Transaction Status</th>");
                sbResult.Append("<td colspan='3' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtTrans.Rows[0]["Status"].ToString() + "</td>");
                sbResult.Append("</tr>");


                sbResult.Append("</table>");
                sbResult.Append("<h4 style='padding-left: 5px;'>Note :-</h4>");
                sbResult.Append("<p style='margin: 0px; padding-left: 5px;'>1. Customer transaction charge is minimum of Rs. 10/- and Maximum 1% of the transaction amount.</p><br />");
                sbResult.Append("<p style='margin: 0px; padding-left: 5px;'>2. In case of non-payment to the beneficiary, the customer will receive an SMS with an OTP that he/she needs to present at the agent location where the transaction was initiated.</p><br />");
                sbResult.Append("<p style='margin: 0px; padding-left: 5px;'>3. In case the Agent charges the Customer in excess of the fee/ charges as mentioned in the receipt, he/she should lodge complaint about the same with our Customer Care on Tel. No. 7409455555 or email us at fastgocash@gmail.com.</p><br />");
                sbResult.Append("<p style='margin: 0px; padding-left: 5px;'>4. The receipt is subject to terms and conditions, privacy policy and terms of use detailed in the website www.fastgocash.com and shall be binding on the Customer for each transaction.</p><br /><br />");

                sbResult.Append("<p style='margin: 0px; padding-left: 5px;'>5. This is a system generated receipt hence does not require any signature.</p><br /><br />");
                sbResult.Append("</div>");
                sbResult.Append("</div>");
            }
        }

        return sbResult.ToString();
    }
    #endregion
}