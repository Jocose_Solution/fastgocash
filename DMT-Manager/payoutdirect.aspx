﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DMT-Manager/MasterPagefor_money-transfer.master" AutoEventWireup="true" CodeFile="payoutdirect.aspx.cs" Inherits="DMT_Manager_payoutdirect" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="custom/css/validationcss.css" rel="stylesheet" />

    <section class="hero-wrap section shadow-md py-4">
        <div class="hero-mask opacity-7 bg-dark"></div>
        <div class="hero-bg" style="background-image: url('images/bg/image-6.jpg');"></div>
        <div class="hero-content py-5">
            <div class="container">
                <div class="row">
                    <div class="col-lg-10 col-xl-10 my-auto" style="margin-left: auto; margin-right: auto;">
                        <div class="bg-white rounded shadow-md p-4">
                            <div class="row">
                                <div class="col-sm-12">
                                    <h3 class="text-5 text-center">Enter Sender’s Mobile Number</h3>
                                </div>
                            </div>
                            <hr class="mb-4" />
                            <div class="row">
                                <div class="col-sm-9 col-lg-offcet-3" style="margin-left: auto; margin: auto;">
                                    <div class="row">
                                        <div class="col-lg-8 form-validation">
                                            <input type="text" id="txtPayoutSenderMobileNo" value="" class="form-control sendmobileno" placeholder="Sender Mobile No." maxlength="10" autocomplete="off" onkeypress="return isNumberValidationPrevent(event);" />
                                            <p style="color: #ccc;">(10 digits) Please don not use prefix zero (0)</p>
                                        </div>
                                        <div class="col-lg-4">                                            
                                            <span id="btnPayoutRemitterMobileSearch" style="cursor: pointer;" class="btn btn-primary btn-block" onclick="return PayoutRemitterMobileSearch();" >Search</span>
                                            <p class="text-danger" id="PayoutTokenExpired"></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="hero-content py-5">
            <div class="container">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="col-sm-12" style="border-radius: 4px; background: #fff; box-shadow: 0px 0px 4px #000000; min-height: 240px;">
                                <br />
                                <h3 class="text-5 text-center">Step 1</h3>
                                <h3 class="text-5 text-center" style="font-size: 18px !important; border-bottom: 1px solid #000;">Register a Sender</h3>
                                <p>Fill in basic information and register a Sender for Money Transfer. Upgrade Sender to KYC by uploading a Photo Id and an Address Proof.</p>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="col-sm-12" style="border-radius: 4px; background: #fff; box-shadow: 0px 0px 4px #000000; min-height: 240px;">
                                <br />
                                <h3 class="text-5 text-center">Step 2</h3>
                                <h3 class="text-5 text-center" style="font-size: 18px !important; border-bottom: 1px solid #000;">Add a Beneficiary</h3>
                                <p>Add multiple Beneficiaries / Receivers against each Sender.</p>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="col-sm-12" style="border-radius: 4px; background: #fff; box-shadow: 0px 0px 4px #000000; min-height: 240px;">
                                <br />
                                <h3 class="text-5 text-center">Step 3</h3>
                                <h3 class="text-5 text-center" style="font-size: 18px !important; border-bottom: 1px solid #000;">Transfer Money</h3>
                                <p>Instantly begin transferring money.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <button type="button" class="btn btn-info btn-lg popupopenclass hidden" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#PopupOpenId"></button>
    <div class="modal fade" id="PopupOpenId" role="dialog">
        <div class="modal-dialog modal-md" style="margin: 15% auto!important;">
            <div class="modal-content" style="text-align: center; border-radius: 5px !important;">
                <div class="modal-header" style="padding: 12px;">
                    <h5 class="modal-title popupheading"></h5>
                </div>
                <div class="modal-body" style="padding-left: 20px!important;">
                    <h6 class="popupcontent"></h6>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" style="padding: 0.5rem 1rem!important;" data-dismiss="modal">OK</button>
                </div>
            </div>
        </div>
    </div>
    <script src="custom/js/payoutdirect.js"></script>
    <script src="custom/js/common.js"></script>
</asp:Content>



