﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DMT-Manager/MasterPagefor_money-transfer.master" AutoEventWireup="true" CodeFile="complete-kyc.aspx.cs" Inherits="DMT_Manager_complete_kyc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="custom/css/dmtdirect.css" rel="stylesheet" />

    <div class="container">
        <div class="row">
            <div id="RemitterDetailsSection" class="col-sm-12" style="border: 1px solid #ccc; padding: 12px; background: #fff; margin-top: 12px; margin-bottom: 12px;">
                <div class="row boxSender">
                    <div class="col-sm-6">
                        <p class="p_botm">Sender Details</p>
                    </div>
                    <div class="col-sm-6 text-right" style="padding: 2px;">
                        <p class="p_botm btn btn-sm" style="border: 1px solid #fff; color: #fff; cursor: pointer; padding: 6px;" id="UpdateLocalAddress"><i class="fa fa-edit"></i>Edit local Address</p>
                    </div>
                </div>
                <div class="row boxSenderdetail">
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-sm-5">
                                <p class="pmargin">Name</p>
                            </div>
                            <div class="col-sm-2">
                                :
                            </div>
                            <div class="col-sm-5">
                                <p class="pmargin">KRISHNA (9555266308)</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-sm-5">
                                <p class="pmargin">Address</p>
                            </div>
                            <div class="col-sm-2">
                                :
                            </div>
                            <div class="col-sm-5">
                                <p class="pmargin">NEW DELHI WEST DELHI DELHI, DELHI, 110059</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-sm-5">
                                <p class="pmargin">Current / Local Address</p>
                            </div>
                            <div class="col-sm-2">
                                :
                            </div>
                            <div class="col-sm-5">
                                <p class="pmargin">NEW DELHI WEST DELHI DELHI</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-sm-5">
                                <p class="pmargin">KYC Status</p>
                            </div>
                            <div class="col-sm-2">
                                :
                            </div>
                            <div class="col-sm-5">
                                <p class="pmargin">PENDING</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <%--saurav code--%>
            <div class="col-sm-12" style="margin-top: 25px; border: 1px solid #ccc; padding: 12px; background: #fff; margin-top: 12px; margin-bottom: 12px;">
                <h3 class="text-center" style="color: #ff414d;">Complete Kyc Form</h3>
                <div class="panel panel-default">
                    <div class="panel-heading">Sender Registration</div>
                    <div class="panel-body">
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label>Mobile Number : <span>9199288931</span></label>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-sm-6">
                                    <label>First Name</label>
                                    <input type="text" class="form-control padding5px" placeholder="Enter first name.." id="fname" />
                                </div>
                                <div class="col-sm-6">
                                    <label>Last Name</label>
                                    <input type="text" class="form-control padding5px" placeholder="Enter last name.." id="lname" />
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-sm-6">
                                    <label>Address as per ID proof</label>
                                    <textarea class="form-control padding5px" rows="3" placeholder="Address as per ID proof" id="address"></textarea>
                                </div>
                                <div class="col-sm-6">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label>Gender</label>
                                            <select class="form-control padding5px" id="gender">
                                                <option value="0">Gender</option>
                                                <option value="male">Male</option>
                                                <option value="female">Female</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-6">
                                            <label>DOB</label>
                                            <input type="text" class="form-control padding5px" placeholder="Select Date" id="DOB" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">ID Proof</div>
                    <div class="panel-body">
                        <div class="col-sm-12">
                            <div class="row form-group">
                                <div class="col-sm-6">
                                    <label>Proof</label>
                                    <select class="form-control padding5px" id="proof">
                                        <option value="0">Select</option>
                                        <option value="Femail">Aadhar card</option>
                                        <option value="Femail">Votar Card</option>
                                    </select>
                                </div>
                                <div class="col-sm-6">
                                    <label>Number</label>
                                    <input type="text" class="form-control padding5px" placeholder="Enter number.." id="number" />
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-sm-6">
                                    <label>Proof Frnt Image</label>
                                    <br />
                                    <input type="file" id="fimage" />
                                </div>
                                <div class="col-sm-6">
                                    <label>Proof Back Image</label>
                                    <br />
                                    <input type="file" id="bimage" />
                                </div>
                            </div>
                            <br />
                            <div class="row form-group">
                                <div class="col-sm-12">
                                    <label style="color: #ff414d;">
                                        <input type="checkbox" class="custom-checkbox" />
                                        By Clicking the checkbox I accept the below declaration</label>
                                </div>
                                <div class="col-sm-12 form-group">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <p style="margin: 0 0px;">1. The Id document has to be marked as "orginal seen and verified"</p>
                                            <p style="margin: 0 0px;">2. Remitter needs to put this stamp on the id document copy.</p>
                                            <p style="margin: 0 0px;">3. The Id document needs to be posted to BAREILLY BAREILLY UTTAR PRADESH, UTTAR PRADESH, 243001 with in 30 days.</p>
                                        </div>
                                        <div class="col-sm-6">
                                            <p style="margin: 0 0px;">4. I here by declare, that the information filled herein above is correct and as per the information provided by the customer.</p>
                                            <p style="margin: 0 0px;">2. The customer has approached me in person for the remittance service.</p>
                                            <p style="margin: 0 0px;">3. The customer ID document have been verified in original.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <input type="button" class="btn btn-primary btn-block" value="Submit" style="width: 25%; margin-left: auto; margin-right: auto;" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>


