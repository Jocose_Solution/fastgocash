﻿<%@ Page Title="" Language="VB" MasterPageFile="~/DMT-Manager/MasterPagefor_money-transfer.master" AutoEventWireup="false" CodeFile="contact-us.aspx.vb" Inherits="DMT_Manager_contact_us" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
	<style type="text/css">
	 /* General styles */

body {
  background-color: #f3f3f3;
  font-family: Futura, sans-serif;
}

.wrapper {
  margin: 24px 180px;
}

h1 {
  color: #716eb6;
  text-align: center;
}

/* Table styles */

table {
  border-collapse: collapse;
  border-spacing: 0;
}

td,
th {
  padding: 0;
  text-align: left;
}

td:first-of-type {
  padding-left: 36px;
  width: 66px;
}

.c-table {
  -moz-border-radius: 4px;
  -webkit-border-radius: 4px;
  background-color: #fff;
  border-radius: 4px;
  font-size: 12px;
  line-height: 1.25;
  margin-bottom: 24px;
  width: 100%;
}

.c-table__cell {
  padding: 12px 6px 12px 12px;
  word-wrap: break-word;
}

.c-table__header tr {
  color: #fff;
}

.c-table__header th {
  background-color: #716eb6;
  padding: 18px 6px 18px 12px;
}

.c-table__header th:first-child {
  border-top-left-radius:  4px;
}

.c-table__header th:last-child {
  border-top-right-radius: 4px;
}

.c-table__body tr {
  border-bottom: 1px solid rgba(113, 110, 182, 0.15);
}

.c-table__body tr:last-child {
  border-bottom: none;
}

.c-table__body tr:hover {
  background-color: rgba(113, 110, 182, 0.15);
  color: #272b37;
}

.c-table__label {
  display: none;
}

/* Mobile table styles */

@media only screen and (max-width: 767px) {

	table, thead, tbody, th, td, tr { 
		display: block; 
  }
  
  td:first-child { 
    padding-top: 24px;
  }

  td:last-child { 
    padding-bottom: 24px;
  }
  
  .c-table {
    border: 1px solid rgba(113, 110, 182, 0.15);
    font-size: 15px;
    line-break: 1.2;
  }

  .c-table__header tr { 
		position: absolute;
		top: -9999px;
		left: -9999px;
	}
  
  .c-table__cell { 
    padding: 12px 24px;
		position: relative; 
    width: 100%;
    word-wrap: break-word;
  }

  .c-table__label {
    color: #272b37;
    display: block;
    font-size: 10px;
    font-weight: 700;
    line-height: 1.2;
    margin-bottom: 6px;
    text-transform: uppercase;
  }
 
  .c-table__body tr:hover {
    background-color: transparent;
  }

  .c-table__body tr:nth-child(odd) {
    background-color: rgba(113, 110, 182, 0.04); 
  }
  
}
	</style>


  <section>
		
	</section>
    
    <section>
		<div class="form form-spac rows con-page">
			<div class="container">
			

		<div class="wrapper">
			<h1>Contact Us</h1>
                        <table class="c-table">
							
								<thead class="c-table__header">
								<tr>
									<th></th>
								<th class="c-table__col-label">Products</th>
									<th class="c-table__col-label">Mobile</th>
									<th class="c-table__col-label">Email</th>
									</tr>
									</thead>
							<tbody class="c-table__body">
								<tr>
									<td>1</td>
									<td class="c-table__cell">For Flights</td>
									<td class="c-table__cell">9760443317</td>
									<td class="c-table__cell">fastgocash@gmail.com</td>
								</tr>
								<tr>
									<td>2</td>
									<td class="c-table__cell">For Domestic Money Transfer</td>
									<td class="c-table__cell">8859999810</td>
									<td class="c-table__cell">fastgocash@gmail.com</td>
								</tr>
								<tr>
									<td>3</td>
									<td class="c-table__cell">For Utilities </td>
									<td class="c-table__cell">9557080160</td>
									<td class="c-table__cell">fastgocash@gmail.com</td>
								</tr>
								<tr>
									<td>4</td>
									<td class="c-table__cell">For Accounts and Payment Upload</td>
									<td class="c-table__cell">7409717717</td>
									<td class="c-table__cell">fastgocash@gmail.com</td>
								</tr>
							</tbody>
                        </table>

			<h1 style="font-size:14px;">Not satisfied with the Services Mobile no 7409455555 and email info@fastgocash.com</h1>
                    </div>				
			</div>
		</div>
	</section>
    
      
    <script type="text/javascript">
        (function () {
            var tableHeaders = document.getElementsByClassName("c-table__header");
            var tableCells = document.getElementsByClassName("c-table__cell");
            var span = document.createElement("span");

            for (var i = 0; i < tableCells.length; i++) {
                span = document.createElement("span");
                span.classList.add("c-table__label");
                tableCells[i].prepend(span);
            }

            var tableLabels = tableHeaders[0].getElementsByClassName("c-table__col-label");
            var spanMod = document.getElementsByClassName("c-table__label");

            for (var i = 0; i < tableLabels.length; i++) {
                for (var a = 0; a < tableCells.length; a++) {
                    spanMod[a].innerHTML = tableLabels[i].innerHTML;
                }
            }

            var b = tableLabels.length;
            for (var a = 0; a < tableCells.length; a++) {
                spanMod[a].innerHTML = tableLabels[a % b].innerHTML;
            }
        })();
    </script>
</asp:Content>

