﻿$(function () {
    $("#txtPayoutSenderMobileNo").val("");
    $("#btnPayoutRemitterMobileSearch").html("Search");
    $("#PayoutTokenExpired").html("");
    $('#txtReAccountNumber').bind('copy paste cut', function (e) { e.preventDefault(); alert("Please enter account number, copy past will not apply!"); });
    BindAllBank();
});

$(document.body).on('click', "#UpdateLocalAddress", function (e) {
    $("#updatemsg").html("");
    $(".updateaddresspopup").click();
});

function UpdateCurrLocalAddress() {
    $("#updatemsg").html("");
    var thisbutton = $("#btnUpdateCurrLocalAddress");
    if (CheckFocusBlankValidation("txtUpdateCurrLocalAddress")) return !1;

    var address = $("#txtUpdateCurrLocalAddress").val();
    if (address != "") {
        $(thisbutton).html("Please Wait... <i class='fa fa-pulse fa-spinner'></i>");

        $.ajax({
            type: "Post",
            contentType: "application/json; charset=utf-8",
            url: "/DMT-Manager/payout_direct_fund_transfer.aspx/UpdateLocalAddress",
            data: '{updatedaddress: ' + JSON.stringify(address) + '}',
            datatype: "json",
            success: function (data) {
                if (data != null) {
                    if (data.d[0] == "success") {
                        $("#txtUpdateCurrLocalAddress").val("");
                        $("#updatemsg").html(data.d[1]);
                    }
                    else {
                        $("#updatemsg").html(data.d[1]);
                    }
                }
                $(thisbutton).html("Update Address");
            },
            failure: function (response) {
                alert("failed");
            }
        });
    }
}

$("#UpdateAddressClose").click(function () {
    window.location.reload();
});

function PayoutRemitterMobileSearch() {
    $("#PayoutTokenExpired").html("");
    var thisbutton = $("#btnPayoutRemitterMobileSearch");
    if (CheckFocusBlankValidation("txtPayoutSenderMobileNo")) return !1;
    var mobileno = $("#txtPayoutSenderMobileNo").val();

    if (mobileno.length == 10) {
        $(thisbutton).html("Please Wait... <i class='fa fa-pulse fa-spinner'></i>");

        $.ajax({
            type: "Post",
            contentType: "application/json; charset=utf-8",
            url: "/dmt-manager/payoutdirect.aspx/RemitterMobileSearch",
            data: '{mobileno: ' + JSON.stringify(mobileno) + '}',
            datatype: "json",
            success: function (data) {
                if (data.d.length > 0) {
                    if (data.d[0] == "success") { window.location.href = data.d[1]; }
                    else if (data.d[0] == "otpsent" || data.d[0] == "registration") {
                        window.location.href = data.d[1];
                    }
                    else if (data.d[0] == "error") {
                        ShowMessagePopup("<i class='fa fa-exclamation-triangle' style='color:#ff414d;' aria-hidden='true'></i>&nbsp;Failed !", "#ff414d", data.d[1], "#ff414d");
                        $(thisbutton).html("Search");
                    }
                    else if (data.d[0] == "reload") { window.location.reload(); }
                }
                else {
                    $("#PayoutTokenExpired").html("Error occured, Token expired !");
                }
                $(thisbutton).html("Search");
            },
            failure: function (response) {
                alert("failed");
            }
        });
    }
    else {
        ShowMessagePopup("<i class='fa fa-exclamation-triangle' style='color:#ff414d;' aria-hidden='true'></i>&nbsp;Failed !", "#ff414d", "Mobile number not valid.", "#ff414d");
    }
}

function PayoutTransFilter() {
    $("#btnTransFilter").html("Searching... <i class='fa fa-pulse fa-spinner'></i>");
    var fromdate = $("#txtTransFromDate").val();
    var todate = $("#txtTransToDate").val();
    var trackid = $("#txtTransTrackId").val();
    var filstatus = $("#ddlTransStatus option:selected").val();
    var type = 'today';

    $.ajax({
        type: "Post",
        contentType: "application/json; charset=utf-8",
        url: "/DMT-Manager/payout_direct_fund_transfer.aspx/GetPayoutTransactionHistory",
        data: '{fromdate: ' + JSON.stringify(fromdate) + ',todate: ' + JSON.stringify(todate) + ',trackid: ' + JSON.stringify(trackid) + ',filstatus: ' + JSON.stringify(filstatus) + ',type: ' + JSON.stringify(type) + '}',
        datatype: "json",
        success: function (data) {
            if (data != null) {
                if (data.d[0] == "success") {
                    $("#PayoutFundTransRowDetails").html(data.d[1]);
                }
            }
            $("#btnTransFilter").html("Search");
        },
        failure: function (response) {
            alert("failed");
        }
    });
}

function BindPayoutTransAllDetails() {
    var fromdate = $("#txtTransFromDate").val();
    var todate = $("#txtTransToDate").val();
    var trackid = $("#txtTransTrackId").val();
    var filstatus = $("#ddlTransStatus option:selected").val();
    var type = 'today';

    $.ajax({
        type: "Post",
        contentType: "application/json; charset=utf-8",
        url: "/DMT-Manager/payout_direct_fund_transfer.aspx/GetPayoutTransactionHistory",
        data: '{fromdate: ' + JSON.stringify(fromdate) + ',todate: ' + JSON.stringify(todate) + ',trackid: ' + JSON.stringify(trackid) + ',filstatus: ' + JSON.stringify(filstatus) + ',type: ' + JSON.stringify(type) + '}',
        datatype: "json",
        success: function (data) {
            if (data != null) {
                if (data.d[0] == "success") {
                    $("#PayoutFundTransRowDetails").html(data.d[1]);
                }
            }
        },
        failure: function (response) {
            alert("failed");
        }
    });
}

$("#PayoutTransHistory").click(function () {
    ClearTransFilter();
});

$("#PayoutDirectTransfer").click(function () {
    ClearPayoutTrans();
});

function ClearTransFilter() {
    $("#btnTransFilter").html("Search");

    $("#txtTransFromDate").val("");
    $("#txtTransToDate").val("");
    $("#txtTransTrackId").val("");
    $("#ddlTransStatus").prop('selectedIndex', '').change();

    $("#PayoutFundTransRowDetails").html("");
    BindPayoutTransAllDetails();
}

function ClearPayoutTrans() {
    $("#txtAccountNumber").val("");
    $("#txtReAccountNumber").val("");
    $("#ddlBindBankDrop").prop('selectedIndex', '').change();
    $("#txtIFSCCode").val("");
    $("#ddlPayoutSp_Key").prop('selectedIndex', 'DPN').change();
    $("#txtBeneficiaryName").val("");
    $("#btnGetBenName").html("Get&nbsp;Name");
    $("#txtAlertMobileNumber").val("");
    $("#txtAlertEmailId").val("");
    $("#txtRemark").val("");
    $("#txtTransferAmount").val("");
    $("#btnPayoutFundTransfer").html("Fund Transfer");
    $("#payoutmessage").html("");
    BindAllBank();
}

function BindAllBank() {
    $("#ProcessBindingBank").removeClass("hidden");
    $.ajax({
        type: "Post",
        contentType: "application/json; charset=utf-8",
        url: "/DMT-Manager/SenderDetails.aspx/BindAllBank",
        //data: '{mobileno: ' + JSON.stringify(mobilenumber) + '}',
        datatype: "json",
        success: function (data) {
            if (data != null) {
                if (data.d != "") {
                    $("#ddlBindBankDrop").html("");
                    $("#ddlBindBankDrop").html(data.d);
                }
            }
            $("#ProcessBindingBank").addClass("hidden");
        },
        failure: function (response) { alert("failed"); }
    });
}

function GetBenNameById() {
    if (CheckFocusBlankValidation("txtAccountNumber")) { $("#txtAccountNumber").prop("placeholder", "Please Enter Account Number"); return !1; }
    if (CheckFocusBlankValidation("txtReAccountNumber")) { $("#txtReAccountNumber").prop("placeholder", "Please Enter Re-Account Number"); return !1; }
    if (CheckSameAccountValidation("txtAccountNumber", "txtReAccountNumber")) return !1;
    if (BankCheckFocusDropDownBlankValidation("ddlBindBankDrop")) return !1;
    if (CheckFocusBlankValidation("txtIFSCCode")) { $("#txtIFSCCode").prop("placeholder", "Please Enter IFSC Code"); return !1; }

    var accountno = $("#txtAccountNumber").val();
    var bankname = $("#ddlBindBankDrop option:selected").text();
    var transfertype = $("#ddlBindBankDrop option:selected").data("transfertype");
    var ifsccode = $("#txtIFSCCode").val();

    if (confirm("Are you sure you want to get beneficiary detail, It will charge ₹ 3 ?")) {
        $("#btnGetBenName").html("featching...<i class='fa fa-pulse fa-spinner'></i>");
        $.ajax({
            type: "Post",
            contentType: "application/json; charset=utf-8",
            url: "/DMT-Manager/SenderDetails.aspx/GetBeneficiaryNamePayout",
            data: '{accountno: ' + JSON.stringify(accountno) + ',transtype: ' + JSON.stringify(transfertype) + ',ifsccode: ' + JSON.stringify(ifsccode) + ',bankname: ' + JSON.stringify(bankname) + '}',
            datatype: "json",
            success: function (data) {
                if (data != null) {
                    if (data.d[0] == "success") {
                        $("#txtBeneficiaryName").val(data.d[1]);
                    }
                    else if (data.d[0] == "reload") {
                        window.location.reload();
                    }
                    else {
                        $("#benerrormessage").html("Error: " + data.d[1]);
                    }
                }
                $("#btnGetBenName").html("Get Name");
            },
            failure: function (response) {
                alert("failed");
            }
        });
    }
}

function ProcessToPayoutFundTransfer() {
    var thisbutton = $("#btnPayoutFundTransfer");

    if (CheckFocusBlankValidation("txtAccountNumber")) { $("#txtAccountNumber").prop("placeholder", "Please Enter Account Number"); return !1; }
    if (CheckFocusBlankValidation("txtReAccountNumber")) { $("#txtReAccountNumber").prop("placeholder", "Please Enter Re-Account Number"); return !1; }
    if (CheckSameAccountValidation("txtAccountNumber", "txtReAccountNumber")) return !1;
    if (BankCheckFocusDropDownBlankValidation("ddlBindBankDrop")) return !1;
    if (CheckFocusBlankValidation("txtIFSCCode")) { $("#txtIFSCCode").prop("placeholder", "Please Enter IFSC Code"); return !1; }
    if (CheckFocusBlankValidation("txtBeneficiaryName")) { $("#txtBeneficiaryName").prop("placeholder", "Please Enter Beneficiary Name"); return !1; }
    if (CheckFocusBlankValidation("txtRemark")) { $("#txtRemark").prop("placeholder", "Please Enter Remark"); return !1; }
    if (CheckFocusBlankValidation("txtTransferAmount")) { $("#txtTransferAmount").prop("placeholder", "Please Enter Transfer Amount"); return !1; }

    var accountno = $("#txtAccountNumber").val();
    var bankname = $("#ddlBindBankDrop option:selected").text();
    var ifsccode = $("#txtIFSCCode").val();
    var transmode = $("#ddlPayoutSp_Key option:selected").val();
    var benname = $("#txtBeneficiaryName").val();
    var alert_mobile = $("#txtAlertMobileNumber").val();
    var alert_email = $("#txtAlertEmailId").val();
    var remark = $("#txtRemark").val();
    var amount = $("#txtTransferAmount").val();

    $("#otpsentmessage").html("");
    $("#fundtransotpmsg").html("");
    $("#txtFundTransOtp").val("");

    if (confirm("Are you sure you want to direct fund transfer?")) {
        $(thisbutton).html("Processing...<i class='fa fa-pulse fa-spinner'></i>");
        $.ajax({
            type: "Post",
            contentType: "application/json; charset=utf-8",
            url: "/DMT-Manager/payout_direct_fund_transfer.aspx/ProcessToFundTransSendOTP",
            data: '{sp_key: ' + JSON.stringify(transmode) + ', amount: ' + JSON.stringify(amount) + '}',
            datatype: "json",
            success: function (data) {
                if (data != null) {
                    if (data.d[0] == "success") {
                        $("#otpsentmessage").html(data.d[1]);
                        $(".fundtransotp").click();
                    }
                    else if (data.d[0] == "reload") {
                        window.location.reload();
                    }
                    else {
                        $("#payoutmessage").html("Error: " + data.d[1]);
                    }
                }
                $(thisbutton).html("Fund Transfer");
            },
            failure: function (response) {
                alert("failed");
            }
        });
    }
}

function ProcessToVerifyFundOtp() {
    var thisbutton = $("#btnFundTransOtpBtn");
    if (CheckFocusBlankValidation("txtFundTransOtp")) { $("#txtFundTransOtp").prop("placeholder", "Please Enter OTP Code"); return !1; }
    var fundotp = $("#txtFundTransOtp").val();
    if (fundotp != "") {
        var accountno = $("#txtAccountNumber").val();
        var bankname = $("#ddlBindBankDrop option:selected").text();
        var ifsccode = $("#txtIFSCCode").val();
        var transmode = $("#ddlPayoutSp_Key option:selected").val();
        var benname = $("#txtBeneficiaryName").val();
        var alert_mobile = $("#txtAlertMobileNumber").val();
        var alert_email = $("#txtAlertEmailId").val();
        var remark = $("#txtRemark").val();
        var amount = $("#txtTransferAmount").val();

        $(thisbutton).html("Verifying...<i class='fa fa-pulse fa-spinner'></i>");
        $.ajax({
            type: "Post",
            contentType: "application/json; charset=utf-8",
            url: "/DMT-Manager/payout_direct_fund_transfer.aspx/ProcessToDirectFundTransfer",
            data: '{accountno: ' + JSON.stringify(accountno) + ',bankname: ' + JSON.stringify(bankname) + ',ifsccode: ' + JSON.stringify(ifsccode) + ',sp_key: ' + JSON.stringify(transmode) + ',benname: ' + JSON.stringify(benname) + ',alert_mobile: ' + JSON.stringify(alert_mobile) + ',alert_email: ' + JSON.stringify(alert_email) + ',remark: ' + JSON.stringify(remark) + ',amount: ' + JSON.stringify(amount) + ',otp: ' + JSON.stringify(fundotp) + '}',
            datatype: "json",
            success: function (data) {
                if (data != null) {
                    if (data.d[0] == "success") {
                        //$("#OTPSentHeading").html("<i class='fa fa-check-circle text-success'></i> Success");
                        //$("#OTPSendContent").html("<h5 class='text-success'>Fund transfer has been successfully<br/>Order Id : " + data.d[1] + "<br/>Client Ref Id : " + data.d[2] + " </h5>");
                        ClearPayoutTrans();
                        $("#otpsucsclose").click();
                        Swal.fire(
                            '<span class="text-success">Success</span>',
                            '<p class="text-success">Fund transfer has been successfully</p><p>Order Id : ' + data.d[1] + '</p><p>Client Ref Id : ' + data.d[2] + '</p>',
                            'success'
                        )
                    }
                    else if (data.d[0] == "reload") {
                        window.location.reload();
                    }
                    else {
                        $("#fundtransotpmsg").html("Error: " + data.d[1]);
                    }
                }
                $(thisbutton).html("Verify OTP");
            },
            failure: function (response) {
                alert("failed");
            }
        });
    }
}

$("#ddlBindBankDrop").change(function () {
    $("#typeofifsc").html("");
    $("#txtIFSCCode").val("");
    var selectedifsccode = $("#ddlBindBankDrop option:selected").val();
    if (selectedifsccode != null && selectedifsccode != "") {
        $("#typeofifsc").html("universal ifsc code.");
        $("#txtIFSCCode").val(selectedifsccode);
    }

    var transfertype = $("#ddlBindBankDrop option:selected").data("transfertype");
    if (transfertype == "IMPS") {
        $("#ddlPayoutSp_Key").html("<option value='BPN'>NEFT</option><option value='DPN'>IMPS</option><option value='CPN'>RTGS</option>");
    }
    else if (transfertype == "NEFT") {
        $("#ddlPayoutSp_Key").html("<option value='BPN'>NEFT</option><option value='CPN'>RTGS</option>");
    }
});

function ShowMessagePopup(headerHeading, headcolor, bodyMsg, bodycolor) {
    $(".popupopenclass").click();
    $(".popupheading").css("color", headcolor).html(headerHeading);
    $(".popupcontent").css("color", bodycolor).html(bodyMsg);
}