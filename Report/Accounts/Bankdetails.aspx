﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageForDash.master" AutoEventWireup="true" CodeFile="Bankdetails.aspx.cs" Inherits="SprReports_Accounts_Bankdetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    <br />
    <br />
    <section class="">

        
        
		<div class="container">
         
			<div class="row">
				<div class="col-md-5 col-sm-5">
                   
					<div class="tourb2-ab-p4-1 tourb2-ab-p4-com hover"> <i><img src="../../Images/Bank/SBILogo_state-bank-of-india-new.png" style="width:50px;"/></i>
						<div class="tourb2-ab-p4-text">
							<h4  style=" margin-top: 0px;margin-bottom: 0px;">STATE BANK OF INDIA</h4>
						
                    <p style="font-size: 12px;"></p>
                            <p style="font-size: 12px;">BANK NAME        :	STATE BANK OF INDIA<br />ACCOUNT NO       :	36009031364<br />IFSC code        :	SBIN0000615</p>
        
                          
						</div>
					</div>
                      
				</div>

                <div class="col-md-5 col-sm-5">
                                   
					<div class="tourb2-ab-p4-1 tourb2-ab-p4-com hover"> <i aria-hidden="true"><img src="https://lh3.googleusercontent.com/sZAzmjgUwDiyEhPe4j-uyGwLDiPcdHdH0n2-BmePP_3IT_wuU2nLKhMKtt6jCX2X-GE" style="width:50px;"/></i>
						<div class="tourb2-ab-p4-text">
							<h4  style=" margin-top: 0px;margin-bottom: 0px;">CENTRAL BANK OF INDIA</h4>
                            <p style="font-size: 12px;"></p>
                            <p style="font-size: 12px;">BANK NAME        :	KOTAK BANK LTD<br />ACCOUNT NO       :	3951682717<br />IFSC code        :		CBIN0280219</p>
						</div>
					</div>
                     
				</div>

                	<div class="col-md-5 col-sm-5">
                    
					<div class="tourb2-ab-p4-1 tourb2-ab-p4-com hover"> <i><img src="../../Images/Bank/ICICI-Bank-PNG-Icon-715x715.png" style="width:50px;"/></i>
						<div class="tourb2-ab-p4-text">
							<h4  style=" margin-top: 0px;margin-bottom: 0px;">ICICI Bank</h4>
							<p style="font-size: 12px;"></p>
                            <p style="font-size: 12px;">BANK NAME        :	ICICI BANK LTD<br />ACCOUNT NO       :	019205006590<br />IFSC code        :			ICIC0000192</p>
						</div>
					</div>
                       
				</div>

				<div class="col-md-5 col-sm-5">                                   
					<div class="tourb2-ab-p4-1 tourb2-ab-p4-com hover"> <i><img src="../../Images/Bank/Hdfc-Logo.png"" style="width:50px;"/></i>
						<div class="tourb2-ab-p4-text">
							<h4  style=" margin-top: 0px;margin-bottom: 0px;">HDFC Bank</h4>
							<p style="font-size: 12px;"></p>
                            <p style="font-size: 12px;">BANK NAME        :	HDFC BANK<br />ACCOUNT NO       :	50200009028805<br />IFSC code        :			HDFC0000304</p>
                           
                            
						</div>
					</div>
                      
				</div>
			
			
				
                <div class="col-md-5 col-sm-5">
                                           
					<div class="tourb2-ab-p4-1 tourb2-ab-p4-com hover"> <i aria-hidden="true"><img src="https://www.logotaglines.com/wp-content/uploads/2016/08/IDBI-Bank-Logo-1200x1185.png" style="width:50px;"/></i>
						<div class="tourb2-ab-p4-text">
							<h4  style=" margin-top: 0px;margin-bottom: 0px;">Bank Of India</h4>
                             <p style="font-size: 12px;"></p>
                            <p style="font-size: 12px;">BANK NAME        :	IDBI BANK<br />ACCOUNT NO       :		0232102000008228<br />IFSC code        :			IBKL0000232</p>
						</div>
					</div>
                     
				</div>
				
			</div>
		</div>

      
     
        

	</section>



    

</asp:Content>

