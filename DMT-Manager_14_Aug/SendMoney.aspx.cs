﻿using InstantPayServiceLib;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class InstantPay_SendMoney : System.Web.UI.Page
{
    private static string UserId { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UID"] != null && !string.IsNullOrEmpty(Session["UID"].ToString()))
        {
            UserId = Session["UID"].ToString();
        }
        else
        {
            Response.Redirect("/");
        }
    }

    private static string BindSenderDetail(dynamic senderDetails)
    {
        StringBuilder sbResult = new StringBuilder();

        try
        {
            dynamic remitter = senderDetails.remitterDetailsData[0].remitter[0];
            // dynamic benificiary = senderDetails.benificiary;
            dynamic remitterLimit = senderDetails.remitterDetailsData[0].remitterLimit[0];

            string senderid = remitter.id;
            string kycStatus = remitter.kycStatus;
            string crditLimit = remitterLimit.limit[0].totalAmount;

            if (!string.IsNullOrEmpty(senderid) && !string.IsNullOrEmpty(crditLimit))
            {
                //sbResult.Append("<div class='col-lg-6 col-xl-6 my-auto' id='SenderDetails'>");
                sbResult.Append("<div class='row no-gutters' style='background: #fff; border-radius: 5px;'>");
                sbResult.Append("<div class='col-sm-5 d-flex justify-content-center bg-primary rounded-left py-4'>");
                sbResult.Append("<div class='my-auto text-center'>");
                sbResult.Append("<div class='text-17 text-white my-3'><i class='fas fa-user'></i></div>");
                sbResult.Append("</div>");
                sbResult.Append("</div>");
                sbResult.Append("<div class='col-sm-7'>");
                //sbResult.Append("<h5 class='text-5 font-weight-400 m-3'>Sender Details</h5>");
                //sbResult.Append("<hr />");
                sbResult.Append("<div class='px-3' style='padding:5px;'>");
                sbResult.Append("<ul class='list-unstyled'>");
                sbResult.Append("<li class='mb-2'>");
                sbResult.Append("<p class='text-muted mb-0'>Name: " + remitter.name + "</p>");
                sbResult.Append("</li>");
                sbResult.Append("<li class='mb-2'>");
                sbResult.Append("<p class='text-muted mb-0'>ID: " + senderid + "</p><input type='hidden' id='hdnRemiterId' value='" + senderid + "' />");
                sbResult.Append("</li>");
                sbResult.Append("<li class='mb-2'>");
                sbResult.Append("<p class='text-muted mb-0'>Credit Limit: ₹" + crditLimit + "</p>");
                sbResult.Append("</li>");
                sbResult.Append("</ul>");
                sbResult.Append("<div>");
                if (kycStatus == "0")
                {
                    sbResult.Append("<span class='bg-danger text-white rounded-pill d-inline-block px-2 mb-1' style='padding: 0.5rem 1rem;border-radius:.2rem!important;'>KYC Status: Pending</span>");
                    //sbResult.Append("<button type='button' class='btn btn-success btn-sm' data-backdrop='static' data-keyboard='false' data-toggle='modal' data-target='#FormRegistration' style='float: right;'>Complete KYC</button>");
                    //sbResult.Append("<span id='btnCompleteKYC' class='btn btn-success btn-sm' style='cursor: pointer;float: right;' onclick='CompleteYourKYC();'>Complete KYC</span>");
                }
                else
                {
                    sbResult.Append("<span class='bg-danger text-white rounded-pill d-inline-block px-2 mb-1' style='padding: 0.3rem;'>KYC Status: Completed</span>");
                }
                sbResult.Append("</div>");

                sbResult.Append("</div>");
                sbResult.Append("</div>");
                sbResult.Append("</div>");
                //sbResult.Append("</div>");
            }
            else
            {
                sbResult.Append(BindRegSec());
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }

        return sbResult.ToString();
    }

    private static string BindBenificiary(dynamic senderDetails)
    {
        StringBuilder sbResult = new StringBuilder();

        try
        {
            var benificiary = senderDetails.remitterDetailsData[0].Value<JArray>("benificiary");
            var bencount = benificiary.Count;

            if (benificiary != null && bencount > 0)
            {
                int counter = 1;
                foreach (var item in benificiary)
                {
                    string status = item.status;

                    sbResult.Append("<div class='transaction-item px-4 py-3'>");
                    sbResult.Append("<div class='row align-items-center flex-row'>");
                    sbResult.Append("<div class='col-2 col-sm-1'><span class='d-block '>" + counter + "</span></div>");
                    sbResult.Append("<div class='col col-sm-2'><span class='d-block '>" + item.name + "</span></div>");
                    sbResult.Append("<div class='col col-sm-2'><span class='d-block'>" + item.account + "</span></div>");
                    sbResult.Append("<div class='col-auto col-sm-2 d-none d-sm-block'><span>" + item.bank + "</span> </div>");
                    sbResult.Append("<div class='col-3 col-sm-2 '><span class='text-nowrap'>" + item.ifsc + "</span></div>");
                    sbResult.Append("<div class='col-3 col-sm-2 '>" + (status == "1" ? "Active" : "DeActive ") + "</div>");
                    sbResult.Append("<div class='col-1 col-sm-1'>");
                    sbResult.Append("<span class='d-block'>");
                    sbResult.Append("<a class='btn-link' data-toggle='collapse' href='#allFilters" + counter + "' aria-expanded='true' aria-controls='allFilters" + counter + "'><i class='fa fa-eye'></i></a>");
                    sbResult.Append("</span>");
                    sbResult.Append("</div>");
                    sbResult.Append("</div>");

                    sbResult.Append("<div class='collapse' id='allFilters" + counter + "'>");
                    sbResult.Append("<div class='card-body'>");
                    sbResult.Append("<div class='col-md-12'>");
                    sbResult.Append("<div class='bg-light shadow-sm rounded p-4 mb-4'>");
                    sbResult.Append("<div class='bl_horizTable bl_horizTable__mdScroll'>");
                    sbResult.Append("<table>");
                    sbResult.Append("<tbody>");
                    sbResult.Append("<tr><th>Beneficiary Name</th><td>" + item.name + "</td></tr>");
                    sbResult.Append("<tr><th>Bank Account</th><td>" + item.account + "</td></tr>");
                    sbResult.Append("<tr><th>Bank Name</th><td>" + item.bank + "</td></tr>");
                    sbResult.Append("<tr><th>Beneficiary Mobile</th><td>" + item.mobile + "</td></tr>");
                    sbResult.Append("<tr><th>IFSC Code</th><td>" + item.ifsc + "</td></tr>");
                    sbResult.Append("<tr><th>Varification Status</th><td>Verified</td></tr>");
                    sbResult.Append("</tbody>");
                    sbResult.Append("</table>");
                    sbResult.Append("</div>");
                    sbResult.Append("<br/><br/>");
                    sbResult.Append("<div class='row'>");
                    sbResult.Append("<div class='col-md-6'>");
                    //sbResult.Append("<button class='btn btn-primary btn-block'><i class='fa fa-trash'></i>&nbsp;&nbsp;Delete Beneficiary</button>");
                    sbResult.Append("<span id='DeleteBen_" + item.id + "' data-benid='" + item.id + "' data-benmobile='" + item.mobile + "' data-benname='" + item.name + "' data-benbank='" + item.bank + "' data-benaccountno='" + item.account + "' class='btn btn-primary btn-block' onclick='DeleteBenEvent(" + item.id + ");'><i class='fa fa-trash'></i>&nbsp;Delete Beneficiary</span>");
                    sbResult.Append("</div>");
                    sbResult.Append("<div class='col-md-6'>");
                    //sbResult.Append("<input type='hidden' data-benificiaryid='" + item.id + "'/>");
                    sbResult.Append("<span id='SendMoney_" + item.id + "' data-benid='" + item.id + "' data-benmobile='" + item.mobile + "' data-benname='" + item.name + "' data-benbank='" + item.bank + "' data-benaccountno='" + item.account + "' class='btn btn-success btn-block' onclick='SendMoneyEvent(" + item.id + ");'>Send Money</span>");
                    sbResult.Append("</div>");
                    sbResult.Append("</div>");
                    sbResult.Append("</div>");
                    sbResult.Append("</div>");
                    sbResult.Append("</div>");
                    sbResult.Append("</div>");
                    sbResult.Append("</div>");
                    counter = counter + 1;
                }
            }
            else
            {
                sbResult.Append("<div class='transaction-item px-4 py-3'>");
                sbResult.Append("<div class='row align-items-center flex-row'>");
                sbResult.Append("<div class='col-12 col-sm-12'>");
                sbResult.Append("<p class='text-center text-danger'>No Beneficiary Details Found!</p>");
                sbResult.Append("</div>");
                sbResult.Append("</div>");
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }

        return sbResult.ToString();
    }

    public static string BindRegSec()
    {
        StringBuilder sbResult = new StringBuilder();

        //sbResult.Append("<div class='col-lg-6 col-xl-6 my-auto' id='SenderDetails'>");
        sbResult.Append("<div class='row no-gutters' style='background: #fff; border-radius: 5px;'>");
        sbResult.Append("<div class='col-sm-5 d-flex justify-content-center bg-primary rounded-left py-4'>");
        sbResult.Append("<div class='my-auto text-center'>");
        sbResult.Append("<div class='text-17 text-white my-3'><i class='fas fa-user'></i></div>");
        sbResult.Append("</div>");
        sbResult.Append("</div>");
        sbResult.Append("<div class='col-sm-7'>");
        sbResult.Append("<h5 class='text-5 font-weight-400 m-3'>Remitter Not Found!</h5>");
        sbResult.Append("<hr />");
        sbResult.Append("<div class='px-3' style='margin-top: 26px;margin-bottom: 26px;'>");
        sbResult.Append("<p>Please register remitter.</p>");
        sbResult.Append("<button type='button' class='btn btn-success btn-sm remitterreg' data-backdrop='static' data-keyboard='false' data-toggle='modal' data-target='#FormRegistration'>Registration</button>");
        sbResult.Append("</div>");
        sbResult.Append("</div>");
        sbResult.Append("</div>");
        //sbResult.Append("</div>");

        return sbResult.ToString();
    }

    [WebMethod]
    public static List<string> RemitterMobileSearch(string mobileno)
    {
        List<string> result = new List<string>();

        try
        {
            string AgentCode = UserId;//"B2B001";
            string Mode = "Web";

            if (!string.IsNullOrEmpty(AgentCode))
            {
                List<string> remtDel = InstantPayFun.GetRemitterDetail(mobileno, AgentCode, Mode);
                if (remtDel != null && remtDel.Count > 0)
                {
                    if (remtDel[0] != "error")
                    {
                        dynamic senderDetails = JObject.Parse(remtDel[1]);
                        string statusCode = senderDetails.statusCode;
                        string statusMessage = senderDetails.statusMessage;

                        if (statusCode == "0" && statusMessage.ToLower() == "success")
                        {
                            result.Add("success");
                            result.Add(BindSenderDetail(senderDetails));
                            result.Add(BindBenificiary(senderDetails));
                            //result.Add(GetMoneyTransferDel(mobileno));
                        }
                        else
                        {
                            result.Add("success");
                            result.Add(BindRegSec());
                        }
                    }
                    else
                    {
                        result.Add(remtDel[0]);
                        if (remtDel[1].Contains("403"))
                        {
                            result.Add("The remote server returned an error: (403) Forbidden");
                        }
                        else
                        {
                            result.Add(remtDel[1]);
                        }
                    }
                }
            }
            else
            {
                result.Add("reload");
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }

        return result;
    }

    [WebMethod]
    public static List<string> RemitterRegistration(string mobile, string firstname, string lastname, string pincode)
    {
        List<string> result = new List<string>();

        try
        {
            string AgentCode = UserId;//UserId;//"B2B001";
            string Mode = "Web";
            if (!string.IsNullOrEmpty(AgentCode))
            {
                List<string> regResult = InstantPayFun.RemitterRegistration(mobile, firstname, lastname, pincode, AgentCode, Mode);
                if (regResult != null && regResult.Count > 0)
                {
                    dynamic regDetails = JObject.Parse(regResult[1]);
                    string statusCode = regDetails.statusCode;
                    string statusMessage = regDetails.statusMessage;

                    if (statusCode == "0" && statusMessage.ToLower().Trim() == "otp sent successfully")
                    {
                        string remiiterId = regDetails.data[0].remiiterId;
                        result.Add("success");
                        result.Add(remiiterId);
                    }
                    else
                    {
                        result.Add("failed");
                        result.Add(statusMessage);
                    }
                }
            }
            else
            {
                result.Add("reload");
            }
        }
        catch (Exception ex)
        {
            result.Add("error");
            result.Add(ex.Message);
        }

        return result;
    }

    [WebMethod]
    public static List<string> RemitterVarification(string mobile, string remitterid, string otp)
    {
        List<string> result = new List<string>();

        try
        {
            string AgentCode = UserId;//UserId;//"B2B001";
            string Mode = "Web";

            List<string> regResult = InstantPayFun.RemitterValidate(remitterid, mobile, otp, AgentCode, Mode);
            if (regResult != null && regResult.Count > 0)
            {
                dynamic regDetails = JObject.Parse(regResult[1]);
                string statusCode = regDetails.statusCode;
                string statusMessage = regDetails.statusMessage;

                if (statusCode == "0" && statusMessage.ToLower().Trim() == "transaction successful")
                {
                    result.Add("success");
                }
            }
        }
        catch (Exception ex)
        {
            result.Add("error");
            result.Add(ex.Message);
        }

        return result;
    }

    [WebMethod]
    public static List<string> BeneficiaryRegistration(string remtmobile, string mobile, string accountno, string ifsccode, string name, string remitterid)
    {
        List<string> result = new List<string>();

        try
        {
            string AgentCode = UserId;//UserId;//"B2B001";
            string Mode = "Web";

            List<string> regResult = InstantPayFun.IPDMTInsertBeneficiary(accountno, ifsccode, name, mobile, remitterid, AgentCode, "MET", Mode);
            if (regResult != null && regResult.Count > 0)
            {
                dynamic regDetails = JObject.Parse(regResult[1]);
                string statusCode = regDetails.statusCode;
                string statusMessage = regDetails.statusMessage;

                if (statusCode == "0" && statusMessage.ToLower().Trim() == "transaction successful")
                {
                    result.Add("success");
                    result.Add(BindBenDelUsingDel(remtmobile));
                }
                else
                {
                    result.Add("failed");
                    result.Add(statusMessage);
                }
            }
        }
        catch (Exception ex)
        {
            result.Add("error");
            result.Add(ex.Message);
        }

        return result;
    }

    [WebMethod]
    public static List<string> DeleteBeneficiary(string remitterid, string benificiaryid)
    {
        List<string> result = new List<string>();

        try
        {
            string AgentCode = UserId;//UserId;//"B2B001";
            string Mode = "Web";

            List<string> regResult = InstantPayFun.IPDMTBenificiaryDeleteDetails(remitterid, benificiaryid, AgentCode, Mode);
            if (regResult != null && regResult.Count > 0)
            {
                dynamic regDetails = JObject.Parse(regResult[1]);
                string statusCode = regDetails.statusCode;
                string statusMessage = regDetails.statusMessage;

                if (statusCode == "0" && statusMessage.ToLower().Trim() == "transaction successful")
                {
                    result.Add("success");
                }
                else
                {
                    result.Add("failed");
                    result.Add(statusMessage);
                }
            }
        }
        catch (Exception ex)
        {
            result.Add("error");
            result.Add(ex.Message);
        }

        return result;
    }

    [WebMethod]
    public static List<string> DeleteBeneficiaryVarification(string mobile, string remitterid, string benificiaryid, string otp)
    {
        List<string> result = new List<string>();

        try
        {
            string AgentCode = UserId;//UserId;//"B2B001";
            string Mode = "Web";

            List<string> regResult = InstantPayFun.IPDMTBenificiaryDeleteVarification(remitterid, benificiaryid, otp, AgentCode, Mode);
            if (regResult != null && regResult.Count > 0)
            {
                dynamic regDetails = JObject.Parse(regResult[1]);
                string statusCode = regDetails.statusCode;
                string statusMessage = regDetails.statusMessage;

                if (statusCode == "0" && statusMessage.ToLower().Trim() == "transaction successful")
                {
                    result.Add("success");
                    result.Add(BindBenDelUsingDel(mobile));
                }
                else
                {
                    result.Add("failed");
                    result.Add(statusMessage);
                }
            }
        }
        catch (Exception ex)
        {
            result.Add("error");
            result.Add(ex.Message);
        }

        return result;
    }

    private static string BindBenDelUsingDel(string mobile)
    {
        string result = string.Empty;

        try
        {
            string AgentCode = UserId;//"B2B001";
            string Mode = "Web";

            //string mobileno = InstantPayFun.GetRemmiterMobileByRemId(remitterid);
            if (!string.IsNullOrEmpty(mobile))
            {
                List<string> remtDel = InstantPayFun.GetRemitterDetail(mobile, AgentCode, Mode);
                if (remtDel != null && remtDel.Count > 0)
                {
                    if (remtDel[0] != "error")
                    {
                        dynamic senderDetails = JObject.Parse(remtDel[1]);
                        string statusCode = senderDetails.statusCode;
                        string statusMessage = senderDetails.statusMessage;

                        if (statusCode == "0" && statusMessage.ToLower() == "success")
                        {
                            result = BindBenificiary(senderDetails);
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }

        return result;
    }

    [WebMethod]
    public static List<string> MoneyTransfer(string remtmobile, string benid, string amount, string txnmode)
    {
        List<string> result = new List<string>();

        try
        {
            if (!string.IsNullOrEmpty(UserId))
            {
                string agencyName = string.Empty; string agencyCreditLimit = string.Empty;

                DataTable dtAgency = InstantPayFun.GetAgencyDetailById(UserId);

                if (dtAgency != null && dtAgency.Rows.Count > 0)
                {
                    agencyName = dtAgency.Rows[0]["Agency_Name"].ToString();
                    agencyCreditLimit = dtAgency.Rows[0]["Crd_Limit"].ToString();

                    if (Convert.ToDouble(agencyCreditLimit) > 0)
                    {
                        string trackid = "DMT" + Guid.NewGuid().ToString().Replace("-", "").Substring(0, 15).ToUpper();

                        SqlTransactionDom objDom = new SqlTransactionDom();
                        int isLedger = objDom.Ledgerandcreditlimit_Transaction(UserId, Convert.ToDouble(amount), trackid, "", "", agencyName, GetLocalIPAddress(), "", "", "", Convert.ToDouble(agencyCreditLimit.Trim()), "Money_Transfer");

                        if (isLedger > 0)
                        {
                            List<string> regResult = InstantPayFun.IPDMTMoneyTransfer(remtmobile, benid, amount, UserId, txnmode, "MET", "WEB", trackid);
                            if (regResult != null && regResult.Count > 0)
                            {
                                dynamic regDetails = JObject.Parse(regResult[1]);

                                string statusCode = regDetails.statusCode;
                                string statusMessage = regDetails.statusMessage;

                                if (statusCode.ToLower() == "0" && statusMessage.ToLower().Trim() == "transaction successful")
                                {
                                    result.Add("success");
                                    //result.Add(BindBenDelUsingDel(mobile));
                                    result.Add(statusMessage);
                                    result.Add(GetMoneyTransferDel(remtmobile));
                                }
                                else
                                {
                                    if (statusCode.ToLower() == "1" && statusMessage.ToLower().Trim() == "amount cannot be less than rs 100.")
                                    {
                                        result.Add("mismatch");
                                        result.Add(statusMessage);
                                    }
                                    else
                                    {
                                        result.Add("failed");
                                        result.Add(statusMessage);
                                    }
                                }
                            }
                        }
                        else
                        {
                            result.Add("failed");
                            result.Add("We are unable to transfer money at the moment. Instead of trying again, please contact our call centre to avoid any inconvenience.");
                        }
                    }
                    else
                    {
                        result.Add("failed");
                        result.Add("Insufficient credit limit !");
                    }
                }
                else
                {
                    result.Add("failed");
                    result.Add("Agency does not exist!");
                }
            }
            else
            {
                result.Add("reload");
            }
        }
        catch (Exception ex)
        {
            result.Add("error");
            result.Add(ex.Message);
        }

        return result;
    }

    public static string GetLocalIPAddress()
    {
        var host = Dns.GetHostEntry(Dns.GetHostName());
        foreach (var ip in host.AddressList)
        {
            if (ip.AddressFamily == AddressFamily.InterNetwork)
            {
                return ip.ToString();
            }
        }
        throw new Exception("No network adapters with an IPv4 address in the system!");
    }

    [WebMethod]
    public static string BindBeneficiaryDetails(string mobile)
    {
        return BindBenDelUsingDel(mobile);
    }

    [WebMethod]
    public static string GetMoneyTransferDel(string rmtmobile)
    {
        string result = string.Empty;

        try
        {
            if (!string.IsNullOrEmpty(UserId))
            {
                result = FillTransDetails(InstantPayFun.GetMoneyTransferDetailsbyAgentId(UserId, rmtmobile));
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }

        return result;
    }

    private static string FillTransDetails(DataTable dtTrans)
    {
        string result = string.Empty;

        try
        {
            StringBuilder sbTrans = new StringBuilder();

            if (dtTrans != null && dtTrans.Rows.Count > 0)
            {
                for (int i = 0; i < dtTrans.Rows.Count; i++)
                {
                    string transid = dtTrans.Rows[i]["FundTransId"].ToString();

                    sbTrans.Append("<div class='transaction-list'>");
                    sbTrans.Append("<div class='transaction-item px-4 py-3' data-toggle='modal' data-target='#transaction-detail_" + transid + "'>");
                    sbTrans.Append("<div class='row align-items-center flex-row'>");
                    sbTrans.Append("<div class='col col-sm-3'><span class='d-block text-3'>" + dtTrans.Rows[i]["TransactionId"].ToString() + "</span><span class='text-muted'>" + ConvertStringDateToStringDateFormate(dtTrans.Rows[i]["CreatedDate"].ToString()) + "</span></div>");
                    sbTrans.Append("<div class='col col-sm-3'><span class='d-block text-2'>" + dtTrans.Rows[i]["bank"].ToString() + "</span><span class='text-muted'>" + dtTrans.Rows[i]["account"].ToString() + "</span></div>");
                    sbTrans.Append("<div class='col-3 col-sm-2  text-4'><span class='text-nowrap'>₹ " + dtTrans.Rows[i]["Amount"].ToString() + "</span></div>");
                    sbTrans.Append("<div class='col-3 col-sm-1'>" + dtTrans.Rows[i]["TxnMode"].ToString() + "</div>");
                    sbTrans.Append("<div class='col-sm-3 text-center'><span class='d-block text-2'>" + dtTrans.Rows[i]["FundTransStatus"].ToString() + "</span><span class='text-muted'>TrackId: " + dtTrans.Rows[i]["TrackId"].ToString() + "</span></div>");
                    sbTrans.Append("</div>");
                    sbTrans.Append("</div>");
                    sbTrans.Append("</div>");

                    sbTrans.Append("<div id='transaction-detail_" + transid + "' class='modal fade' role='dialog' aria-hidden='true'>");
                    sbTrans.Append("<div class='modal-dialog modal-dialog-centered transaction-details' role='document'>");
                    sbTrans.Append("<div class='modal-content'>");
                    sbTrans.Append("<div class='modal-body'>");
                    sbTrans.Append("<div class='row no-gutters'>");

                    sbTrans.Append("<div class='col-sm-5 d-flex justify-content-center bg-primary rounded-left py-4' style='background: #a9a9a9 !important;'>");
                    sbTrans.Append("<div class='my-auto text-center'>");
                    sbTrans.Append("<div class='text-17 text-white my-3'><i class='fas fa-building'></i></div>");
                    //sbTrans.Append("<h3 class='text-4 text-white font-weight-400 my-3'>Envato Pty Ltd</h3>");
                    sbTrans.Append("<div class='text-8 font-weight-500 text-white my-4'>₹ " + dtTrans.Rows[i]["Amount"].ToString() + "</div>");
                    sbTrans.Append("<p class='text-white'>" + dtTrans.Rows[i]["TrackId"].ToString() + "</p>");
                    sbTrans.Append("<p class='text-white'>" + ConvertStringDateToStringDateFormate(dtTrans.Rows[i]["CreatedDate"].ToString()) + "</p>");
                    sbTrans.Append("</div>");
                    sbTrans.Append("</div>");

                    sbTrans.Append("<div class='col-sm-7'>");
                    sbTrans.Append("<h5 class='text-5 font-weight-400 m-3'>Transaction Details");
                    sbTrans.Append("<button type='button' class='close font-weight-400' data-dismiss='modal' aria-label='Close'>");
                    sbTrans.Append("<span aria-hidden='true'>×</span>");
                    sbTrans.Append("</button>");
                    sbTrans.Append("</h5><hr />");
                    sbTrans.Append("<div class='px-3'>");
                    sbTrans.Append("<ul class='list-unstyled'>");
                    sbTrans.Append("<li class='mb-2'>Payment Amount <span class='float-right text-3'>₹ " + dtTrans.Rows[i]["Amount"].ToString() + "</span></li>");
                    //sbTrans.Append("<li class='mb-2'>Fee <span class='float-right text-3'>-₹4.80</span></li>");
                    sbTrans.Append("</ul>");
                    sbTrans.Append("<hr class='mb-2' />");
                    sbTrans.Append("<p class='d-flex align-items-center font-weight-500 mb-4'>Total Amount <span class='text-3 ml-auto'>₹ " + dtTrans.Rows[i]["Amount"].ToString() + "</span></p>");
                    //sbTrans.Append("<ul class='list-unstyled'>");
                    //sbTrans.Append("<li class='font-weight-500'>Paid By:</li>");
                    //sbTrans.Append("<li class='text-muted'>Envato Pty Ltd</li>");
                    //sbTrans.Append("</ul>");
                    sbTrans.Append("<ul class='list-unstyled'>");
                    sbTrans.Append("<li class='font-weight-500'>Refrence Id:</li>");
                    sbTrans.Append("<li class='text-muted'>" + dtTrans.Rows[i]["IpayReferenceId"].ToString() + "</li>");
                    sbTrans.Append("</ul>");

                    sbTrans.Append("<ul class='list-unstyled'>");
                    sbTrans.Append("<li class='font-weight-500'>Refrence Number:</li>");
                    sbTrans.Append("<li class='text-muted'>" + dtTrans.Rows[i]["IpayRefNumber"].ToString() + "</li>");
                    sbTrans.Append("</ul>");

                    sbTrans.Append("<ul class='list-unstyled'>");
                    sbTrans.Append("<li class='font-weight-500'>Operator ID:</li>");
                    sbTrans.Append("<li class='text-muted'>" + dtTrans.Rows[i]["IpayOprId"].ToString() + "</li>");
                    sbTrans.Append("</ul>");

                    sbTrans.Append("<ul class='list-unstyled'>");
                    sbTrans.Append("<li class='font-weight-500'>Transaction ID:</li>");
                    sbTrans.Append("<li class='text-muted'>" + dtTrans.Rows[i]["TransactionId"].ToString() + "</li>");
                    sbTrans.Append("</ul>");

                    sbTrans.Append("<ul class='list-unstyled'>");
                    sbTrans.Append("<li class='font-weight-500'>Track ID:</li>");
                    sbTrans.Append("<li class='text-muted'>" + dtTrans.Rows[i]["TrackId"].ToString() + "</li>");
                    sbTrans.Append("</ul>");

                    sbTrans.Append("<ul class='list-unstyled'>");
                    sbTrans.Append("<li class='font-weight-500'>Trancation Mode:</li>");
                    sbTrans.Append("<li class='text-muted'>" + dtTrans.Rows[i]["TxnMode"].ToString() + "</li>");
                    sbTrans.Append("</ul>");
                    //sbTrans.Append("<ul class='list-unstyled'>");
                    //sbTrans.Append("<li class='font-weight-500'>Description:</li>");
                    //sbTrans.Append("<li class='text-muted'>Envato March 2019 Member Payment</li>");
                    //sbTrans.Append("</ul>");
                    sbTrans.Append("<ul class='list-unstyled'>");
                    sbTrans.Append("<li class='font-weight-500'>Status:</li>");
                    sbTrans.Append("<li class='text-muted'>" + dtTrans.Rows[i]["FundTransStatus"].ToString() + "</li>");
                    sbTrans.Append("</ul>");
                    sbTrans.Append("</div>");
                    sbTrans.Append("</div>");
                    sbTrans.Append("</div>");
                    sbTrans.Append("</div>");
                    sbTrans.Append("</div>");
                    sbTrans.Append("</div>");
                    sbTrans.Append("</div>");
                }

                result = sbTrans.ToString();
            }
            else
            {
                sbTrans.Append("<div class='transaction-list'>");
                sbTrans.Append("<div class='transaction-item px-4 py-3'>");
                sbTrans.Append("<div class='row align-items-center flex-row'>");
                sbTrans.Append("<div class='col-sm-12 text-center text-danger'>No Trancations Found!</div>");
                sbTrans.Append("</div>");
                sbTrans.Append("</div>");
                sbTrans.Append("</div>");

                result = sbTrans.ToString();
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }
        return result;
    }

    [WebMethod]
    public static string FilterTransDetails(string fromdate, string todate, string transid, string trackid)
    {
        string result = string.Empty;

        try
        {
            if (!string.IsNullOrEmpty(fromdate) || !string.IsNullOrEmpty(todate) || !string.IsNullOrEmpty(transid) || !string.IsNullOrEmpty(trackid))
            {
                if (!string.IsNullOrEmpty(UserId))
                {
                    result = FillTransDetails(InstantPayFun.IPDMTFilterTransDetails(UserId, fromdate, todate, transid, trackid));
                }
                else
                {
                    result = "reload";
                }

            }
            else
            {
                result = "empty";
            }
        }
        catch (Exception ex)
        {
            ex.ToString();
        }

        return result;
    }
    
    private static string GetDateFormated(string date)
    {
        DateTime dtDate = new DateTime();

        if (!string.IsNullOrEmpty(date))
        {
            dtDate = DateTime.Parse(date);

            string thisday = dtDate.Day.ToString();
            string thismonth = dtDate.Month.ToString("MM");

            return "<span class='d-block text-4 font-weight-300'>" + thisday + "</span> <span class='d-block text-1 font-weight-300 text-uppercase'>" + thismonth + "</span>";
        }

        return string.Empty;
    }

    public static string ConvertStringDateToStringDateFormate(string date)
    {
        DateTime dtDate = new DateTime();

        if (!string.IsNullOrEmpty(date))
        {
            dtDate = DateTime.Parse(date);
            return dtDate.ToString("dd MMM yyyy hh:mm tt");
        }

        return string.Empty;
    }
}