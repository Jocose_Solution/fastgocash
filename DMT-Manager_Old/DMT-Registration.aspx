﻿<%@ Page Title="" Language="VB" MasterPageFile="~/DMT-Manager/MasterPagefor_money-transfer.master" AutoEventWireup="false" CodeFile="DMT-Registration.aspx.vb" Inherits="DMT_Manager_DMT_Registration" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="container">
        <div class="login-signup-page mx-auto my-5">
            <h3 class="font-weight-400 text-center">Sign Up for DMT</h3>
            <p class="lead text-center">Your Sign Up information is safe with us.</p>
            <div class="bg-light shadow-md rounded p-4 mx-2">
                <form id="signupForm">
                    <div class="form-group">
                        <label for="fullName">Mobile No.</label>
                        <input type="text" class="form-control" runat="server" id="fullName" required="" placeholder="Enter Mobile No." />
                    </div>
                    <div class="form-group">
                        <label for="firstname">First Name</label>
                        <input type="email" class="form-control" runat="server" id="emailAddress" required="" placeholder="Enter First Name" />
                    </div>
                    <div class="form-group">
                        <label for="loginPassword">Surname</label>
                        <input type="password" class="form-control" runat="server" id="loginPassword" required="" placeholder="Enter Surname" />
                    </div>

                    <div class="form-group">
                        <label for="loginPassword">PIN Code</label>
                        <input type="password" class="form-control" runat="server" id="Password1" required="" placeholder="Enter PIN" />
                    </div>
                    <div class="form-group">
                        <label for="loginPassword">Outlet ID</label>
                        <input type="password" class="form-control" runat="server" id="Password2" required="" placeholder="Enter Outlet ID" />
                    </div>

                    <%--<button class="btn btn-primary btn-block my-4" type="submit" onclick="">Sign Up</button>--%>
                    <asp:Button runat="server" ID="btn_Registration" class="btn btn-primary btn-block my-4" OnClick="btn_Registration_Click" />
                </form>
                <p class="text-3 text-muted text-center mb-0">Already have a DMT account? <a class="btn-link" href="login-3.html">Send Money</a></p>
            </div>
        </div>
        <!-- Content end -->


    </div>





</asp:Content>

